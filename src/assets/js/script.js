
var hyphenatorSettings = {
  hyphenchar : 		'-',
  urlhyphenchar : 	'',
  minwordlength : 2
};
Hyphenator.config(hyphenatorSettings);


$('#text').keyup(function(){
  var text = $('#text').val();
  text = text.replace(/(?:\r\n|\r|\n)/g, ' ');
  text = Hyphenator.hyphenate(text, 'de');

  var words = text.split(' ');

  var silben = [];
  for (var i = 0; i < words.length; i++){
    var wort = words[i];
    var wortssilben = wort.split('-');
    silben = silben.concat(wortssilben);
  }
  $('#output').html('');
  for (var i = 0; i < silben.length; i++){
    var html = [];
    var chars = silben[i].split('');
    for(var j = 0; j< chars.length;j++){
        html.push('<div class="character character-'+j+' character-'+chars[j].toLowerCase()+'" data-number="'+j+'">'+chars[j]+'</div>');
    }

    $('#output').append('<div class="icon">'+html.join('')+'</div>');
  }
  $('.character').each(function(){
    $(this).rotate($(this).data('number')*45,1000);
  });
});

$('#text').keyup()
